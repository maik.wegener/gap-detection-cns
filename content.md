# Gap detection using computer vision

## Problem formulation
- how to use computer vision to detect different types and sizes of gaps

## State of the art methods
- human inspections
- 3d models

## Paper: GapFlyt: Active Vision Based Minimalist Structure-less Gap Detection For Quadrotor Flight
- problem: quadrotor should be able to fly through gaps
  - currently done via 3D model of the environment
    - many resources needed, not mountable on a quatrodor
- idea: insects don't need such a 3D model
  - approach: detecting and flying through gaps using only a monocular camera and on board sensing

### Algorithm
#### TS²P
- depth estimation by moving in certain way
  - quadrotor movements are controlled -> easier interpretations of optical flow
- optical calculated for background and foreground seperately via FlowNet2

- image from the monocular camera seperated in different zones/regions
  - F(oreground)
  - B(ackground)
  - U(ncertain)
    - brighter: active region of frame
    - darker: inactive region of frame

- contour tracking using $`F \cup B`$ -> no aggresive maneuvers possible due to no actual direct contour tracking 

- quadrotor represended as ellipsoid
  - for this paper: it is assumed, that the gap is large enough for the quadrotor to fly through
- Safe region $`S = \mathcal{O} \ominus \mathcal{Q}`$ (where $`\mathcal{O}: \text{opening frame;} \mathcal{Q}: \text{ellipsoid representation of quadrotor}`$)
  - safest point $`x_s`$ is defined as center of largest eclipse, fitting S
  - alignment of body center to $`x_s`$

### Experiments
- Parrot Bebop 2 (front facing camera, 9-axis IMU (inertial measurement unit), downward facing optical flow sensor with sonar)
- NVIDIA Jetson TX2 GPU on top of quadrotor, communication via WiFi (30Hz)
- Fig 6
- prototyping of experiments done on a powerful PC (MATLAB Robotics Toolbox)
- optical flow based on deep learning using Python and TensorFlow back-end and ported to L4T (Linux for Tegra)
- four different tests:
  - (1) different sizes and shapes of opening
  - (2) noise sensitivity
  - (3) outputs for different values of N (frames), image baselines and image sizes
  - (4) alternative approaches (state of the art) to find the gap

### Results
- overall results for TP²S: 85% successful flights over 150 trials
- (1) gap gets detected over various different sizes and shapes (Fig 8)
- (2) shows, that dynamic noises are handled well by the algorithm (Fig 9)
- (3) shows, that even low-res cameras can be used (32x48px) (Fig 12, 13)
- comparison (4): Fig 10 (gap detection), Fig 11 (optical flow), Table II (numbers)

### Conclusion
- TS²P can be used for gap finding with a minimal setup
- parting thought: coupling IMU with camera to get a scale of the opening frame for more aggresive maneuvers


## Paper: Vision-Based Fatigue Crack Detection of Steel Structures Using Video Feature Tracking
- fatigue cracks on steel bridges (due to repetetive, high load) are a real problem
  - currently detected via human inspection
    - cost intensive, time intensive and unreliable
- idea/approach: using small video clips and computer vision to detect and track fatigue tracks

### Algorithm
- camera in front of a girder takes multiple short video streams of the structural surface under load
- overall algorithm: feature point detection -> feature point tracking -> crack detection and localization -> crack opening quantification
  - all feature points in same movement -> no crack
  - some feature points in different movement -> possible crack

#### Feature point detection
- Fig 3
- Shi-Tomasi algorithm is used because of its robust performance
- ROI (region of interest) is selected first
- feature points determinition is done using matrix calculation, eigenvalues and cutoff threshold
- feature points detection features different aspects
  - based on change of intensity gradient
  - no special lightning or high end cameras needed
  - many feature points are detected in small ROI

#### Feature point tracking
- adapted Kanade-Lucas-Tomasi tracker used (KLT)
- movement is estimated with Gaussian window function
- KLT needs constant lightning and surface texture conditions (given due to short videos)

#### Crack detetection and localization
- Fig 4
- discontinuities in surface motion are detected
- coefficient of calculation computer -> exceeds given threshold -> possible crack
- repeated for all feature points

#### Quantification of the crack opening 
- peak to peak amplitude of crack movement
- one windows deployed on each side of crack
  - movement between windows = (movement of upper windows) - (movement of lower window)
- each window contains 5 feature points at least

### Experiments and results

#### Test configuration
- Fig 5
- Nikon Camera in front of CT specimen (25 cm distance)

#### Crack detection
- Fig 6/7
- 6 sec Video (FHD, 29.97 FPS) video taken and processed using matlab and the given approach
- crack successfully detected

#### Quantification of the crack opening 
- Fig 8
- windows(60x60) are deployed on each side of the crack path for measurment
- vision-based results agree on the ground truth

#### Validation for out-of-plane fatigue crack
- Fig 10
- skewed bridge girder, mounted on floor, cross frame attached to girder
- camera at 15cm distance
- out-of-plane crack detected as above 

### Discussion of test results
#### Radius (r) of LCR (localized circular region) and coefficient of variation $`\Gamma`$
- r:
  - increasing: more cracks detected, less accuraty
  - decreasing: more accurate, less robust (less cracks detected)
- $`\Gamma`$:
  - increasing: smaller feature points cloud, more cracks detected, less robust
  - decreasing: bigger feature points cloud, less cracks detected, less accurate
- finetuning needed

#### other results
- approach is very robust and can be used for long term monitoring (influence of conditions is very small, even works after changing lightning, camera distance)
- proposed approach is more robust as methods, which are based on static images (e.g. Sobal and Canny edge detection), which need further processing due to detecting a lot of edge-like structures
- no false positives
- computational cost: 1 minute of processing time for 6 sec,FHD video on normal desktop computer (16GB ram, 3.1GHz CPU)

#### Limitations: 
- camera lense needs to be...
  - stabilized during filming
  - parallel to monitored surface
- object of known size needed for scaling

## Paper: Abnormality detection strategies for surface inspection using robot mounted laser scanners
- detecting small abnormalities on large, form-free surfaces is a real problem (e.g. car production)
  - currently detected via human inspection 
     - cost intensive, time intensive, limited in accuracity
- idea/approach: using a robot-mounted laser scanner to detect such abnormalities

### Alias
- PC = point cloud
- H-H = highly curved artefact
- L-L = less curved artefact


### Algorithm
- rough idea:
  - scanning surfaces using laser scanners
  - creating PCs based on scans
  - detecting gaps in PCs as abnormalities

### Experiments
#### Materials used
- two different types of artefacts used
  - planar square shape (6x6cm, 5x5 matrix of abnormalities)
  - aluminium curved surface objects (20x20cm)
- different laser scanners for different artefact-types, both mounted on robot arm
  - Micro epsilon scanner for planar square object
  - custom laser based on Flexpint MVnano and Basler acA1600-20gm used for curved objects
- 2D laser profiles are created from the scans
  - Fig 2.4 for micro epsilon scanner
    - PCs degrade severely for last two angles, poor at 15°
    - Fig 2.5 -> abnormalities form gap in scan lines
    - Fig 2.6 -> two smallest rows of abnormalities cannot be seen at 5° angle
  - custom scanner
    - dents and bumps shown in Fig 2.7
    - Fig 2.8 -> PCs and some abnormalities
    - inconsistent line spacing
    - abnormalities may be missed, if spacing between lines is > 1mm
- noise needs to be removed
  - low threshold, but noise removal strategies are applied
- angle between camera is also very important
  - epsilon scanner: camera angle is fixed to scanner setup -> blackbox
  - custom made scanner: 30° camera angle, laser almost orthogonal to surface

#### Data analysis
##### Unsupervised analysis
- for planar artefact (micro epsilon scanner)
- first: computational analysis of PC quality is done
  - based on completeness and noise
- pre processing noise using MATLAB 'pcdenoise'
- second: abnormality detection
  - abnormalities form gaps over scan lines
  - abnormality detected if distance between two points (=gap) is above threshold
- false positives are less critical than false negatives

#### Supervised analysis
- for curved aluminium objects
- PCs are noisier 
- shapes of abnormalities are more complex
  - two thirds of PC data is labelled manually for training, rest is used for testing
- algorithm in rough(Fig 3.1):
  - pre processing (noise removal)
  - feature extraction and classification training
  - testing

- pre processing:
  - noise removal using MATLAB 'pcdenoise' (as above), removes some abnormalities too
  - more noise is removed using another algorithm (TODO: describe)

- feature extraction: following features are extracted:
  - normal features (see 3.5)
  - height features
  - chi-squared statistics (how well does it fit the gaussian distribution) (eq 3.4)
  - proximity features 
  - (feature transformation & training)

### Results
#### Unsupervised results
- quality computation:
  - PC in 25°, 20° and 15° are not considered for detection analysis due to bad results
  - Table 4.1
- abnormality detection results:
  - factors varied: change in size of abnormalities, threshold, line resolution, projection angle
  - Fig 4.2 -> detected abnormalities at 5° of different sizes vurses line resolution (low threshold leads to more FP)
  - Fig 4.3 -> ROC curve for 5° (similar trends for 0° and 10°)
    - small threhols -> TPR and FPR increases (worst case: FPR = 1)
    - compromise for threshold: above abnormality size of last two rows 
  - Fig 4.4 highest TPR, lowest FPR at 5°; highest TNR, lowest FPR at 0°, second best at 5°
  - higher resolutions -> better results
##### Discussion on results
- key factors are threshold value and laser projection angle
- not possible to detect abnormalities from last two rows (Fig. 4.1)
- Fig 5.1 shows detected abnormalities on different thresholds
- scanner position relative to surface is important (best compromise at 5°)


#### supervised results
- high-noise regions found and excluded, rest of noise is filtered and included
- pre-defined features were used for training
  - classification using Rayleigh Quotient worked really well (H-H classification performance = 100%, L-L classification performance = 98.913)
- test step results:
  - first, detection of suspicious points via thresholding
  - second, classification of abnormalities 
  - very good results for both PCs (no FP for L-L, one FP for H-H)

##### Discussion on results
- some normal points marked as suspicious using the unsupervised thresholding strategy
  - classification using features needed 
  - all classification should be used for both PCs
  - Fig 5.2 -> performance analysis
- inspection speed: 500mm²/s
  - optimizable through more integration of the robot and the laser scanner
  - trained classification model leads to fast data analysis -> test algorithm runs 3.35s for 150k points

## Conclusion

## Future Works
