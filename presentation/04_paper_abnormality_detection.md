##### 23 - Sectionpage - Abnormality Detection Strategies for Surface Inspections 

##### 24 - Problem & basic idea
- abnormalities on surfaces are a real problem, e.g. in car production
    - abnormality on free form surfaces is difficult
    - done by human inspection
        - expensive
        - inaccurate 
            - abnormalities are very small

##### 25 - Problem & basic idea
- idea:
    - using a robot mounted laser scanner to detect these abnormalities
    - can be automated

##### 26 - Problem & basic idea 
- algorithm in short:
    1. scanning the surface using a laser scanner
    2. creating point clouds (PC) based on scans
    3. detecting gaps in point clouds as abnormalities

##### 27 - Experiments & Results (unsupervised analysis)
- unsupervised analysis used for planar artefact
- Experiments:
    1. creating point cloud from planar artefact 
        - image shows point clouds for 5° and 10° projection angle
            - point clouds degrade severely an angle of 15° or more
    2. denoising the point cloud using MATLABs 'pcdenoise' filtering
    3. abnormality detection
        - image shows gaps formed by abnormalities for 5° and 10° projection angle

##### 28 - Experiments & Results (unsupervised analysis)
- results:
    - 5° is best projection angle for scanning
        - image shows detection results for abnormalities of 572µm and 182µm size
            - higher resolution -> more abnormalities detected
            - lower threshold -> more abnormalities detected
            - low threshold leads to many false positives
        - image shows TPR/FPR curve for different linespaces
            - markers show different threshold values
            - circle > square > diamond > pentagram
            - last two markers (diamond and pentagram) are overlaid due to close values of last threshols and resulting TPR/FPR

##### 29 - Experiments & Results (supervised analysis)
- supervised analysis for curved aluminium artefacts
- Experiments: 
    1. creating point cloud from artefact
    2. excluding high-noise regions 
        - see image
    3. feature extraction & training using labelled points (2/3 of PC are labelled)
        - normal features
        - height features (height of abnormality)
        - $`\chi^2`$ statistics (ability to fit gaussian distribution)
        - proximity features
    4. test step using unlabelled points 

##### 30 - Experiments & Results (supervised analysis)
- results:
    - classification worked very well
        - performance for highly curved artefact: 100% 
        - performance for less curved artefact: 99.913% 
    - test step results:
        - 0 false positive for less curved artefact
        - 1 false positive for highly curved artefact
    - image shows FN/FP/performance for different features
        - all features necessary to get good results

