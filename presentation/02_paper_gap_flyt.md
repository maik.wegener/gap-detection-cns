##### 06 - Problem & basic idea (1/2)
- quadrotor should fly through a gap
    - normally, a 3D map is needed for such a task
    - computation of 3D map is not possible on the quadrotor itself
        - too heavy, too big
- test setup
    - upper right corner: camera view

##### 07 - Problem & basic idea (2/2)
- inspired by insects
    - they perform such tasks easily without a 3D map
- challenge is: flying through gaps with only a monocular camera
- on-board sensing, so that everything needed can be mounted on the quadrotor
- quadrotor is a Parrot Bebop 2 (camera (1), 9-axis IMU, optical flow sensor with sonar (3))
- (2) is NVIDIA Jetson TX2, communication via wifi at 30Hz

##### 08 - Algorithm: $`TS^2P`$
- $`TS^2P`$ stands for Temporally Stacked Spatial Parallax
- Algorithm uses movement control and monocular camera for gap detection
- camera image gets divided into F(oreground), B(ackground), U(ncertain zone)
- points in each zone get scores:
    - F: x = +1
    - B: x = -1
    - U: $`x \in (-1, 1)`$ 
    - C: x = 0
    - contour tracking is done indirectly via corner tracking (fast)
        - this only gives approximate contour
        - no aggressive maneuvers are possible
        - for this paper: assumed, that the gap is big enough for the quadrotor
- safest point $`x_s`$ is computed and then followed
    - $`x_s`$ is defined as center of largest eclipse fitting the gap

##### 09 - Experiments & Results (1/2)
- testing for 
    - different size or shapes of opening
        - as visible, different sizes/shapes are no problem for the algorithm
    - noise senitivity
        - noise increases (up to down)
        - gap gets detected even in the last, noisiest image 

##### 10 - Experiments & Results (2/2)
- testing for 
    - different framecount, image baselines/resolutions
        - first image: resolutions: 384x576, 96x144, 32x48
            - gap detection without a problem
        - second image: different frame counts: 2 frames, 4 frames, 8 frames used for input
            - best result at 8 frames, but gap gets detected even with only 2 frames
    - comparison to alternative approaches: 
        - TS^2 with FlowNet2 is slowest
        - TS^2 with FlowNet has best detection rate
        - TS^2 with FlowNet2 has best average false negative rate
        - DSO/Stereo SLAM have best average false positive rate
- overall success rate for TS^2P is 85%

