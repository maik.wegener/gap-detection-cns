##### 01-Title

- Maik Wegener
- Gap Detection Using Computer Vision

##### 02 - Outline
- Problem formulation
- State of the art methods
- Different Approaches for Different Problems
    - Paper: GapFlyt: Minimalist Gap Detection for Quadrotor Flight
    - Paper: Vision-Based Fatique Crack Detection of Steel Structures
    - Paper: Abnormality Detection Strategies for Surface Inspections
- Conclusion
- Future Works

##### 03 - Problem formulation
How computer vision can simplify the detection of very different gaps

##### 04 - State of the art methods
- crack and abnormality detection is done via human inspection
    - inefficient
    - expensive
        - highly trained people needed
        - specialist are always expensive
        - inspection itself is complicated and takes time
    - unreliable, chance of error
        - can lead to dangerous cracks 

- 3D model is computed for quadrotor flight
    - high computational load
        - takes time
    - needs fully powered PC
        - not portable
    - expensive due to needed hardware

##### 05 - A closer look on three papers:
- GapFlyt: discussion of possibility to fly through gaps, using a minimalist setup
- Crack Detection: video based approach for detecting fatigue cracks in steel structures (e.g. bridges)
- Abnormality Detection: exploring the possibility to detect abnormalities on different artefacts (e.g. car parts)
