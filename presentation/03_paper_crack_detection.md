##### 11 - Sectionpage - Vision-Based Fatigue Crack Detection of Steel Structures

##### 12 - Problem and basic idea (1/3)
- steel structures get fatigue cracks 
    - due to high, repetetive load (through cars, trucks)
    - detected by human inspection
        - time intensive 
            - few inspectors are checking all the steel girders of one bridge
        - expensive 
        - unreliable 
            - cracks are sometimes hard to detect for human eyes

##### 13 - Problem and basic idea (2/3)
- idea:
    - detect and track fatigue cracks with short video clips (6 seconds)
        - camera is place in front of steel girder
        - takes multiple short videos under load 
            - crack is visible under load
    - would be much cheaper, if the approach can be automated

##### 14 - Problem and basic idea (3/3)
- algorithm in short:
    1. feature point detection
    2. feature point tracking
    3. crack detection and localization
    4. crack opening quantification 
    - if the feature move differently, a crack is possible 
        - further processing needed

##### 15 - Algorithm
- feature point detection:
    - using Shi-Tomasi algorithm:
        1. region of interest is selected
        2. feature points are selected using matrix calculation and eigenvalues
            - feature points are based on change of intensity gradient
            - no special lightning or high end cameras needed
            - many feature point even in small region of interest

##### 16 - Algorithm
- feature point tracking:
    - adapted Kanade-Lucas-Tomasi tracker is used 
    - movement is estimated with gaussian window function
    - tracker needs constant lightning and surface texture conditions
        - given due to short videos

##### 17 - Algorithm 
- crack detection:
    - crack is possible, when surface motion discontinuities are deteced
        - checked with given threshold
    - test is repeated for all feature points
    - figure shows: 
        - movement of Point 21 - 24 is equal, 25/26 are moving in different direction 
            - crack detected

##### 18 - Algorithm
- crack opening quantification:
    - windows are deployed on both sides of the crack
    - movement between windows = movement of upper window - movement of lower window
    - each window contains at least 5 feature points

##### 19 - Experiments & Results
- test setup: 
    - CT specimen on which load gets applied
    - camera mounted in front of CT specimen 
        - picture 2 shows camera view
    - speciman already got a crack, shown in picture 4

##### 20 - Experiments & Results
- crack detection results
    - crack clearly visible under load
    - region of interest selected
    - feature points selected
    - crack tip detected

##### 21 - Experiments & Results
- crack opening quantification:
    - windows deployed (60 x 60)
    - crack opening visible in chart
    - vision based results agree on ground truth

##### 22 - Experiments & Results
- testing algorithm against out-of-plane cracks
    - skewed bridge girder, mounted on floor, cross frame atteched to girder
    - camera at 15cm distance
    - crack is hard to see for human eyes
    - crack gets detected by approach 

- overall results:
    - approach is very robust and can be used for long term monitoring
        - even works after changing lightning conditions and camera distance
    - approach is more robust than static-image based approaches
    - no false positives
    - 1 minute of computational cost for processing 6 seconds of FHD video (normal desktop computer)

(add discussion on radius of LCR and coefficient of variation, if presentation is too short)


    
