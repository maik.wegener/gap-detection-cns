##### 31 - Conclusion
- computer vision can help detecting all different types of gaps
- specialization for the task is needed 
    - 3 tasks shown, 3 very different approaches used
- sometimes computer-vision based methods are limited by the environment
    - light conditions
- even computer-vision based approaches aren't free from error
    - but in most cases more reliable than human inspections

##### 32 - Future Works
- gap detection on quadrotor:
    - coupling IMU (inertial measurement unit) data with the monocular camera to get a scale of the gap and plan for more aggressive maneuvers 
    - main limitation named by paper

- crack detection using videos
    - implementing the approach on autonomous platforms for automated structural inspections
    - much easier than manual, human inspections

##### 33 - Question Time
