# Vision-Based Fatigue Crack Detection of Steel Structures Using Video Feature Tracking - Summary 

## Abstract
- fatigue cracks on steel bridges are a problem
- human inspection is used to detect these cracks
  - slow, labor intensive, unreliable
- possible solution: computer-vision based crack detection
  - using consumer-grade camera
  - surface is tracked under repetitive load
  - algorithm detects cracks through differential changes
- effectiveness of this approach is tested on two different experiments
  - in-plane cracks and out-plane cracks
  - results indicate that the approach can detect cracks even under bad circumstances

## Introduction
- infrastructures are very important
  - but can take structural damage due to loads
  - structural health monitoring (SHM) techniques have been studied
- many steel highway bridges get fatigue cracks
  - slow in the beginning -> hard to detect early -> develop rapidly -> catastrophic failures
- currently most bridge owners run human-driven routine inspections at least every 24 months
  - time consuming, labor intensive, cost ineffective, prone to error
- non-destructive testing (NDT) techniques (acoustic emission, etc) can improve accuracy
  - additional power needed, increasing monitoring complexity 
- strain-based monitoring techniques detect fatigue cracks by abrupt strain change due to cracking
  - extra work on sensor installation is needed 
- computer vision based crack detection methods have shown great potential 
  - low-cost, easy to deploy, contactless
- most common approach: image processing technique (IPT) 
- early IPT-based technique feature edge detection, percolation processing, sequential image filtering etc
  - rely on edge features -> true cracks and crack-like edges can't be distinguished
- advanced IPT-base techniques have been developed
  - based on features of pavement cracks
- deep learning with a large volume of images of concrete cracks have been developed
  - image filters are used to eliminate edge-like structures from possible cracks
  - detecting true fatigue crack is still challenging
- all of above IPT-based techniques use static features
- approach: track and analyze the surface in motion with Digital Image Correlation by tracking discontinuity of displacement field
  - expensive equipment needed
- approach: using video-based techniques
  - all currently known video-based approaches haven't been used for detecting fatigue cracks in steel struktures
- this article presents a computer-vision based approach to detect fatigue cracks through a video stream
  - method tracks surface motion and detects cracks based on discontinuities by opening and closing of fatigue cracks under repetitive loads
  - methods doesn't rely on edge detection -> results are more robust

## Methodology
### Overview:
- consumer-grade digital camera deployed in front of girder
- multiple short video streams under load are collected (covering the entire structural surface)
- overall methodology: feature point detection   - feature point tracking -> crack detection and localization -> crack opening quantification
  - all feature points share similar movement -> no crack
  - some feature points have different movement -> possible crack 

### Feature point detection:
- automatic detection without surface treatment or physical targets
- Shi-Tomasi algorithm is used in this study (robust performance)
- there are other algorithms available, which could also have been used
- Region of interest (ROI) is selected first
  - can be subset or entire scene, depending on prior crack knowledge
- feature point determinition using matrix calculation, eigenvalues and a cutoff threshold
- Shi-Tomasi demonstrated in Fig 3
- detecting feature points features the following aspects:
  - based on change of intensity gradient
  - do not require special lightning or high end cameras to be detected
  - many feature points are detected in small ROI

### Feature point tracking:
- study uses adapted Kanade-Lucas-Tomasi tracker (KLT)
- KLT would estimate movement of feature point i between two adjacent frames within Gaussian window function
  - sum of squared differences is minimized
- forward and backward tracking is also adopted to detect tracking failures
- KLT needs constand lightning and surface texture conditions
  - given due to short videos

### Crack detection and localization algorihm:
- principle: locate efficiently and robustly discontinuities in surface motion
- coefficient of variation CV^i is computed
  - smaller than predefined threshold -> rigid-body movement -> no crack
  - exceeding given threshold -> differential movement patterns -> possible crack
- procedure is repeated for all feature points
  - highlighted -> indicator of crack path
- see Fig 4 for demonstration

### Crack opening quantification:
- defined as peak-to-peak amplitude of crack movement
- based on crack detection and localization algorithm pairs of small windows are deployed along both sides of the crack
  - by substracting the movement of the top windows from the bottom windows the relative movement between the windows can be obtained
- to ensure the accuracy, every window should contain at least 5 feature points

## Experimental Validation
### Test Configuration:
  - see Fig 5
  - Nikon camera, in front of a CT specimen (25cm distance)
  - fatigue crack has been generated in the specimen through periodic load
  - crack is opening due to higher loads

### Crack detection:
  - 6-second video (FHD, 29,97FPS) was taken and processed using mathlab computer vision toolbox and the approach
  - see Fig 6/7
  - crack got detected (7c)

### Quantification of the crack opening
  - see Fig. 8
  - two windows (60x60) are deployed on two locations, one on each side of the crack path
  - ground truth and vision-based measurements agree excellently 

#### Validation for out-of-plane fatigue crack
##### Test config:
  - Figure 10
  - skewed bridge girder, mounted on the floor, cross frame attached to girder

  - regular ceiling lights
  - camera 15cm in front of crack
  - video taken while applying loads

##### crack detection like above
  - out-of-plane crack got detected

### Discussion of test results
#### Parametric study for r and \R
  - increasing r -> more robust crack detection, but accuracy is limited
  - smaller r -> accurate crack detection, but less robust
  - increasing \R -> smaller feature point cloud
  - smaller \R -> bigger feature point cloud, accuracy of crack path may decrease
  - finetuning of r and \R is necessary, optimal ranges vary with different applications

#### Robustness:
  - the approach is very robust, works even after changing all conditions (lighning, camera position, etc)
  - useful as long term monitoring tool

#### Comparative study:
  - comparision with Sobal and Canny edge detection methods, based on static images
  - both detect the crack, but also a lot of other edge-like structures -> further processing is needed
  - proposed approach has more robust detection results (no false positives)

#### Computational cost:
  - 1 minute of processing time needed for 6 seconds of 1920x1080 video on a normal desktop computer (16 GB RAM, 3.1GHz CPU) ? Cores not specified

#### Limitations:
  - camera needs to be stabilized during video filming
  - camera lens needs to be parallel to monitored surface
  - known object is needed for scaling

## Conclusion
- vision based approach for automated fatigue crack detection from video stream is presented
  - based on tracking surface motion of cracked smecimen under repetitive loads
- effectiveness has been successfully validated throught two test setups
  - tests imply that fatigue cracks can be robustly identified by the proposed approach
- approach is cost effective
- limitations have been discussed
- Future studies: focus on implementation of the algorihm on autonomous platforms (UAVs) for automated structural inspections
