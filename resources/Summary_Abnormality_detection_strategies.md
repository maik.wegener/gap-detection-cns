# Abnormality detection strategies for surface inspection using robot mounted laser scanners - Summary

## Abstract
- detection of small surface abnormalities on large complex free-form surface is significant challenge
  - surface abnormalities are often less than a millimeter square and must be located on surfaces of multiple meters square
- robotic or automated inspection systems are highly desirable
  - must be robust and accurate
  - robot-mounted lasers scanners use point clouds (PC) for measurement
- paper addresses challenge of surface abnormality detection using PC
  - first, threshold stategy is considered and analyzed by experiment
  - second, abnormality detection strategy is proposed and tested
- results show that laser projection angle and line spacing are important
- compromise between threshold value and sensitivity should be made

## 1. Introduction
- Quality control is essential for manufacturing operations
  - detecting surface abnormalities is challenging part of it
  - e.g. for car bodys
- traditionally inspections are carried by human experts
  - effective at locating abnormalities, but limited in accuracy, consistency, speed and reliability
- early inspection technology was based on image analysis 
  - lack of 3D local information
  - stereo vision systems can work with 3D localisation, but are not appropriate for flat surfaces
  - light scanners avoid this issue by projectinga pattern of light/dark on the surface
- one successful technology is usage of laser line scanning technology
  - low weight, compact size
  - easily integrated into robots, for flexible inspection system 
  - large quantities of data can be quickly collected
  - processing/analysis is still challenging

### 1.1. Research so far
- detection based on existing CAD model
- detection using noise removal method
- detection using octree structures and recursive subdivision
- limitations of theses methods remain unclear
- techniques have been studied for detection of large abnormalities of a few cm in size on conrete surfaces
- there are no studies considering very small abnormalities (µm/mm) in shiny metal surfaces

- paper addresses the problem of abnormality detection on freeform metal surfaces using PCs from robot-mounted laser scanners
- successful abnormality detection depends on 
  - detection algorithm
  - shape and size of abnormalities
  - quality of the acquired PC
  - scan conditions
  - choice of supervised or unsupervised detection strategy can also be important
- paper starts with analysing impact of these criterias
- then a supervised abnormality detection algorithm is considered

## 2. Materials and methods
### 2.1. Artefacts
- two different types of artefacts
  - planar square shape (6x6cm, 5x5 matrix of abnormalities)
    - diamond shape cavities; diameter 700µm -> 550 µm -> 180 µm -> 120 µm
  - aluminium curved surface objects (two pieces, 20cm x 20cm)
    - different cuvature levels
    - bumps and dents from 700µm - 1mm diameter

### 2.2. Laser scanners
- two different laser scanners are used

#### 2.2.1. Micro epsilon laser scanner
- accurate commercial laser scanner
  - consistent of laser light source, sensor matrix
  - see Fig 2.1
  - projects diffusely reflected laser light onto a sensor matrix
    - output on a two-dimensional coordinate system

#### 2.2.2. Custom made laser scanner
- for scanning the aluminium objects, a custom made laser scanner is used
- Flexpoint MVnano, 450nm, 1 mW, 30° fan angle, focusable
- Basler acA1600-20 gm GigE camera
- resolution is leass than resolution of micro epsilon laser scanner 

#### 2.2.3. Robot
- both lasers are mounted on a Fanur LR Mate iC robot arm
- see Fig 2.2
- for scanning aluminium objects robot path was chosen, suc that the laser scanner was normal and at same stand-off distance, allowing scanning of large objects with curvature and height variation
- scan was also performed with fixed step size on curved object

### 2.3. Data sets
- 2D laser profiles from the camera sensor are used
  - camera, laser, robot calibration information as well as 3D transformations related to robot movement are utilized

#### 2.3.1. Planar artefact data
- 2D profile results from Micro Epsilon scanner are transformed into robot coordinate system
- see Fig 2.4
- resulting PCs degrade severely for last two angles, poor at 15°
- Fig 2.5 shows enlarged abnormalities, forming a gap in scan lines
- Fig 2.6 compares abnormalities of different sizes at 5° projection angle
  - two smallest abnormalities cannot be seen

#### 2.3.2. Aluminium objects data
- scanned using custom laser scanner
- dents and bumps as shown in Fig 2.7
- Fig 2.8 shows PCs and some abnormalities 
- line spacing is not constant in both PCs (see Fig 2.7b)
- an abnormality should be coincident with at least two or three lines in most parts of the PCs
  - may be missed if located in few number of outlying lines with more than 1mm spacing

### 2.4. Noise and uncertainties
- three different sources of inaccuracity, resulting in gaps and holes in a PC
  - reflection from surface
  - environmental effects
  - shadowing when an object lies in path of laser line
- these effects can be alleviated by choice of camera positions and angle and by controlling environmental conditions
- additionally any scanning progress parameter (aliasing effects, etc.) can influence PC quality
- noise is reduced by choice of threshold value for laser line segmenation
  - high value -> useful data may be discarded
  - low value -> noisier PC
- low threshold values are used
  - noise removal stategies are employed
- angle between camera and surface is also important
  - angle of epsilon scanner is considered as black box, as the camera angle is fixed in the scanner setup
    - tested to find best angle for abnormality detection
  - custom made laser scanner has camera angle of 30°, laser is almost orthogonal to surface 

## 3. Data analysis
- unsupervised approach for the planar artefact
- supervised strategy for more realistic scenario

### 3.1. Unsupervised analysis
- first, quality evaluation analysis for planar artefact is described 
- then unsupervised abnormality detection strategy is explained step by step

#### 3.1.1. Computational analysis of PC quality
- PC quality evaluated based on completeness and noise
  - completeness measurement:
    - missing data points divided by total data points 
  - noise computation:
    - PCs divided into 2x2mm patches
    - each patch has limit of 20 data points
    - more than 20 points on PC -> patch is considered for noise evaluation

#### 3.1.2. Pre-processing
- smoothing filter is used for micro epsilon laser data
- MATLAB 'pcdenoise' was used

#### 3.1.3. Unsupervised abnormality detection 
- abnormalities of planar form gaps over scan lines
- Euclidean distances between adjacent point i, j in each line is computed and assigned to abnormal class C_{abnormal}_ if distances are higher than the threshold (see 3.1 and 3.2)
- PC obtained by Micro Epsilon scanner consist of more than 1.5 million data points
  - PC is subdivided into groups that represent a surface patch of 2x2mm
  - abnormality detection is made for each individual patch
- subsampling of lines is performed to see how resolution in lateral space affects abnormality detection

#### 3.1.4. Performance evaluation
- perfomance is based on detection of true positives 
- false positives is less critical than false negatives
- receiving operating characteristics ROC (receiving operating characteristics) curve is computed based on true positive rate as well as true negative rate (see eq 3.3)

### 3.2. Supervised analysis
- PCs are acquired from curved aluminium objects and therefore noisier with a varying level of noise and lateral line spacing
- shapes of abnormalities are more complex
- supervised strategy requires data dividing PC data into two classes
  - labelled manually 
  - two thirds of each PC for training, rest for testing
- proposed algorithm can be described as three main steps:
  - pro processing for smoothing and highly noise data exclusion
  - feature extraction and classification model training
  - test step
  - see Fig 3.1

#### 3.2.1. Pre-processing
- fine smoothing to remove undesired points around PCs using MATLAB 'pcdenoise'
  - some features associated with abnormalities are removed due to this
  - see Fig 3.2
- fine smoothing does not remove more significant noise due to low threshold level
- noise can be missclassified as real abnormalities 
- more noise is removed using the algorithm described on P7 (65)

#### 3.2.2. Feature extraction
- PC abnormalities are not all in similar shape
- labeled abnormal regions as well as some points from different normal regions of the training area are considered equally
- rectangle patch of points is considerer around each point

##### 3.2.2.1. Normal features 
- surface is fitted to patch points
  - normal is computed
  - average coordinate of all points in patch
  - angle between normal and connecting vector is considered
- see Fig 3.5

##### 3.2.2.2. Height features
- difference between original height of points and corresponding height in fitted plane
- height changes can be considered at each line within the patch

##### 3.2.2.3. Chi-squared statitics
- abnormal points have similar behaviour to gaussian distribution
- goodnes to fit to gaussian is computed using eq 3.4

##### 3.2.2.4. Proximity features
- to detect abnormalities which are hard to detect due to missing points in PC lines a proximity feature is needed
  - based on maximum distance of each point in a patch from its nearast point in the adjacent lines

##### 3.2.2.5. Feature transformation
- robust classification models are trained based on discriminative features
- features of normal and abnormal classes should be well seperated and overlap as small as possible
- to increase the distance of differen features Rayleigh Quotient strategy is employed
- eigenvectors are used to maximize different features from each other

##### 3.2.2.6. Training a classification model 
- SVM classifier is trained using the above transformed features
  - SVM s kernel-based classification method, based on maximum margin algorithm
  - appropriate for data sets with linear or non-linear behaviour
- folding is used to find the optimal model
- LibSVM toolbox for MATHLAB is used for SVM algorithm

#### 3.2.3. Test step
- PC is on scale of few hundred thousand points
- to reduce computational load, a thresholding strategy is applied on each PC
- result of this unsupervised step provides all suspicious points, including abnormalities (TP), noisy and missing point areas (FP)
- analysis steps, including patching, filtering, feature extraction, only performed on a very limited set of data

## 4. Experimental results
### 4.1. Unsupervised analysis results
#### 4.1.1. Quality computation results
- PC in 25° angle could not be analyzed
- Table 4.1 shows results of other angles
- all factors increase with increased angle
- 15° and 20° angle misses too many points -> not considered for abnormality detection analysis

#### 4.1.2. Abnormality detection results
- four different factors were varied
  - systematic change in sizes of abnormalities (Fig. 4.1.)
  - threshold for unsupervised detection algorithm
  - line resolution (step size)
  - projection angle
- Fig 4.2 shows detected abnormalities at 5°
  - detection process improves for higher resolutions and smaller threshold
  - low threshold -> more FP
- ROC curve for 5° shown in Fig 4.3
  - small threshold -> TPR and FPR increases
    - worst case: low res + low threshold -> FPR = 1 (all normal patches are detected as abnormalities)
  - compromise for threshold is needed
    - threshold low enough to detect last two rows (4.1) of abnormalities leads to a lot of FP -> threshold must be set to not detect these sizes
  - similar trends for 0° and 10°
- combined effects of projection angle and line resolution are shown in Fig 4.4
  - highest TPR, lowest FPR
    - best results obtained at 5°, second best at 10°
    - worst result at 0°
  - highest TNR, lowest FPR (classification of normal points)
    - best results at 0°, second best at 5°
  - higher resolutions -> better results (for TPR, FNR; FPR and TNR are also higher)

### 4.2. Supervised analysis results
#### 4.2.1. Pre-processing results
- noise detection algorithm found areas of high noise level
- high-noise regions are exluded
- other noise is filtered with the adaptive mean filter and included

#### 4.2.2. Feature extraction and training step results
- features from 3.2.2 used for training
- for H-H PC, maximum point between nearest point in adjacent lines was also used
- see Fig. 4.7 for in depth view on the used features
- after using Rayleigh Quotient for incresing distances of features from two classes are well seperated 
  - leading to 100% classification performance and 98.913% for training based on labelled data of H-H/L-L PCs

#### 4.2.3. Test step results
- first suspicious points are detected using the unsupervised thresholding
- second final abnormalities were classified usind the above detected points
- programm found all abnormalities successfully for both PCs
  - no FP for L-L
  - one FP for H-H

## 5. Discussion
### Unsupervised results
- key factors: threshold value, laser projection angle
- Micro Epsilon scanner showed that:
  - small thresholds -> high sensitivity, but decreased specificity
  - compromise for threshold needed
- due to limits in scanner resolution it's impossible to detect the two smallest sized abnormalities
- Fig 5.1 shows detected abnormalities at each row of planar object 
  - abnormalities can be detected, if threshold is at least less than the defect size
- position of scanner relative to surface is also important
  - angle needs to be considered
- best PC quality achieved at 0°, but worst abnormality detection results
  - due to diamon shaped abnormalities
  - better visibility at 5°
- best FPR using 0°
- 5° is best compromise for optimal TPR and FPR rates

### Supervised results
- unsupervised thresholding strategy leads to some normal points detected as suspicious
  - important role of feature extraction
  - classification model was trained using 
    - two normal features
    - two height features
    - chi-squared feature (H-H, L-L)
    - proximity feature (H-H)
  - performance analysis (Fig 5.2) shows that all features should be used for both PCs
    - each feature characterizes an unique aspect of the abnormalities
- speed of inspection system: 500mm^2/s
  - can be optimized through more integration of the robot and the laser scanner
  - use of high frequency scanners would also speed things up
  - previously trained classification model leads to fast data analysis
  - running time for test algorithm is 3.35s for 150 thousand data points

## 6. Conclusion
- two data analysis strategies for surface abnormality detection were presented
- results show, that threshold value of 225µm is the best compromise for best TPR/FPR
- robustness can not be achieved using only a unsupervised thresholding strategy
  - but it helps to reduce the computational load
- 5° projection angle leads to best results
- training classification performance was 100% and 98.913% for the two PCs
- all abnormalities were found in the test step, with only one FP on one of the two tested PCs
- 
