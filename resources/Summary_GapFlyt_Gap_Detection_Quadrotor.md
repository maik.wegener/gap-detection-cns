# GapFlyt: Active Vision Based Minimalist Structure-less Gap Detection For Quadrotor Flight - Summary

## Abstract:
- currently 3D reconstructions of the scene are used for quadrotors
  - ineffecient (not task driven, not used by flying insects/birds)
  - flying insects/birds don't need such a complex structure
- this paper covers the approach of using bio-inspired perceptual design
  - minimalist sensori-motor framework without 3D reconstruction 
  - using only a monocular camera and onboard sensing

## Intruduction and philosophy:
- for autonomy quad need to have perceptual capabilities to sense the world and react 
- 3D models are very useful for this, but cannot be used on the quadrotor itself due to weight, size, 
- approach in this paper is limited to a monocular camera and IMU (inertial sensor)
- trying to recover a minimal amount of information needed to perform ceveral tasks (see Table I)
  - using "active" vision, inspired by the fruit fly, which does not perceive depth directly
- paper focuses on obstactle avoidance 
  - evaluates visual mathematics, tries to demonstrate the approach for different set ups

## Gap detection using TS^2P
- depth estimation problem can be trivialized by moving in a certain way
  - quadrotors movements are controlled to make interpretations of optical flow easier
- optical flow can be modelled as (P. 3, lower left math)
  - this equation can be used for foreground and background independently
- paper uses FlowNet2 to compute optical flow
- noise reduction: mean of flow magnitudes across a few frames is computed 
- possible boundary regions: (mean flow magnitude, F) - (mean flow magnitudes, B)
  - using the inverse flow differences gives better numerical stability and better noise performance 
- same as solving the edge detection problem in computer vision

## High speed gap tracking for visual servoing based control
- Focus of Expansion constraints:
  - pixel on location x is associated with score _x \in [-1, 1] (1 -> F, -1 -> B, 0 -> _x < 0)
  - pixels which cannot be classified belong the uncertainy zone U (_x \in (-1, 1))
  - contour location is defined as (_x = 0)
  - see Fig. 4
- problem: tracking of contour location across time, relies on _x in U
  - corner tracking using set of F \cup B -> trade-off: no agressive maneuvers are possible (not important for this paper), see Algorithm 1
A:
- quadrotor represented as ellipsoid with semi-axes of lengths a,b and c
- _Q   - projection of quatrotor on the image
- Safe region S can be computed as S = \bigcup _O \ominus _Q
- safest point x_s defined as the center of the largest ellipse, which fits inside S
- it is assumed, that the gap is large enough for the quadrotor to pass through
  - given that, the safe point computation can be performed as median of convex set _O and is given by x_s \approx argmin_x \sum_{\forall o \in \mathcal(O)} || o - x ||_2
- safest point is computed as [math on P. 5, left column, center]
- Fig 5 shows switching between x_s,F and x_s,B
- x_s jumps around due to resetting and active switching   - solved with a kalman filter
B:
- controller contains: inner loop and outer loop controllers
  - inner loop: controls attidute stability
  - outer loop: controls quadrotor position
- control policy is to align the projection of the body center on the image plane with x_s
- difference between two centers is the error (e)
- control policy only deals with the alignment of B to x_s and not with moving forward

## Experiments
### Setup
- Parrot Bebop 2 as quadrotor, with front facing camera, 9-axis IMU and downward facing optical flow sensor, coupled with a sonar
- NVIDIA Jetson TX2 GPU mounted on the Bebop 2 used to run all perception and control algorithms onboard
- communication between quadrotor and TX2 via WiFi (images at 30Hz)
- prototyping is done on a very powerful PC, using the Robotics Toolbox
- optical flow is based on deep learning using Python and the TensorFlow back-end
- final versions of software were ported to Python to run on the TX2 running Linux (L4T)
- envitonmental setup: see Fig. 2
### Results:
- first test (1): using different shapes and sizes of the opening
- second test (2): noise sensitivity of TS^2P
- third test (3): outputs for different values of N (frames), image baselines and image sizes
- fourth test (4): presents alternative approaches (state of the art methods) to find the gap
- (3) shows, that even using a low resolution camera (32x48 px) can be used with the algorithm
- dynamic noises are also handled very well by the algorithm 
- using TS^2P results in a eroded version of the true gap
  - good for safe maneuvers, bad for aggresive flight maneuvers
- (4) uses untrained and unrefined deep learning methods
  - gap detection results: see Fig. 10 and Table II 
  - optical flow results: see Fig. 11
- overall results for TP^2S: 85% over 150 trials

## Conclusion
- minimalist philosophy to mimic insect behaviour to solve complex problems
  - used to develop for gap finding with a monocular camera and onboard sensing
- first paper which addresses the problem of gap detection using this minimal approach
- parting thought: IMU data can be coupled with the camera to get a scale of the windows and plan aggresive maneuvers
