# Resources regarding 'Gap Detection Using Computer Vision'

### Papers
[GapFlyt](https://arxiv.org/pdf/1802.05330.pdf)  
[Abnormality detection strategies for surface inspection using robot mounted laser scanners](https://www.sciencedirect.com/science/article/pii/S0957415818300370)  
[Vision‐Based Fatigue Crack Detection of Steel Structures Using Video Feature Tracking](https://onlinelibrary.wiley.com/doi/pdf/10.1111/mice.12353)  

### Summaries of papers (so far)
[GapFlyt](Summary_GapFlyt_Gap_Detection_Quadrotor.md)  
[Abnormality detection strategies for surface inspection using robot mounted laser scanners](Summary_Abnormality_detection_strategies.md)  
[Vision‐Based Fatigue Crack Detection of Steel Structures Using Video Feature Tracking](Summary_Crack_Detection_Steel_Structures.md)  

