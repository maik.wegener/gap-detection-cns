# Presentation regarding 'Gap Detection Using Computer Vision'

Build: [![pipeline status](https://gitlab.gwdg.de/maik.wegener/gap-detection-cns/badges/master/pipeline.svg)](https://gitlab.gwdg.de/maik.wegener/gap-detection-cns/commits/master)  
[Compiled presentation](https://gitlab.gwdg.de/maik.wegener/gap-detection-cns/-/jobs/artifacts/master/raw/gap_detection.pdf?job=compile_pdf)  
## Resources used

### Papers
[GapFlyt](https://arxiv.org/pdf/1802.05330.pdf)  
[Abnormality detection strategies for surface inspection using robot mounted laser scanners](https://www.sciencedirect.com/science/article/pii/S0957415818300370)  
[Vision‐Based Fatigue Crack Detection of Steel Structures Using Video Feature Tracking](https://onlinelibrary.wiley.com/doi/pdf/10.1111/mice.12353)  

### Other resources:
[LaTeX theme](https://github.com/matze/mtheme)  
[LaTeX CI](https://hub.docker.com/r/paperist/alpine-texlive-ja/)  
